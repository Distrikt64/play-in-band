# Play In Band

![](https://img.shields.io/badge/Unity-2019.1.9f1-%23000.svg?style=flat-square&logo=unity)
![](https://img.shields.io/badge/Version-v1.0.0-%23f54242.svg?style=flat-square)
![](https://img.shields.io/badge/Licence-CC_BY_NC_SA-%230b4.svg?style=flat-square)

Jeu 3D musical minimaliste en VR multijoueur réalisé avec Unity dans le cadre des projets tuteurés en licence pro DAWIN à l’IUT de Bordeaux. Ce jeu a été réalisé pour le casque VR Oculus Quest.

![Scene du jeu](https://i.imgur.com/npNW5lM.png)

## Release

Vous trouverez à la racine du projet, un dossier nommé "Releases" qui contient la dernière version du jeu, qui a été utilisée pour la démo Twitch. Vous trouverez plus bas comment l'installer sur le Quest.

### Installation de l'APK

Si vous voulez installer l'APK disponible dans le dossier release, vous devez utiliser un sideloader comme [SideQuest](https://github.com/the-expanse/SideQuest). L'utilisation de ce logiciel requiert le mode développeur sur le Quest. Le jeu sera ensuite disponible sur l'Oculus Quest depuis Bibliothèque  > Sources inconnues > Play In Band.



## Installation et compilation

### Configuration Minimum
- Unity 2019.1.9f1
- Un Oculus Quest !

### Récupération du projet

Clonez le projet et ouvrez le avec Unity.

### Compatibilité Android

La première chose à faire est d'indiquer à Unity que l'on veut compiler pour Android (l'OS qui tourne sur le Quest). Dans les paramètres de build, cliquez sur "Android" puis sur "Switch Platform"

Toujours dans la même fenêtre, pour l'option "Texture Compression" choisissez "ASTC".

### Plugin Oculus (VR)

Le jeu étant réalisé pour l'Oculus Quest, nous avons utilisé l’intégration Oculus pour Unity.
Vous devez donc l'installer depuis [ici](https://assetstore.unity.com/packages/tools/integration/oculus-integration-82022).


Redémarrez Unity et installez les plugins demandés.

Pour configurer le plugin correctement, vous devez disposer d'un AppID. La création d'une organisation et d'une application dans le dashboard développeur d'Oculus ([ici](https://dashboard.oculus.com/)) est nécessaire pour un bénéficier d'un.

Après avoir récupéré votre AppID, renseignez le dans les paramètres Oculus Avatar et Oculus platform settings dans le menu du haut d'Unity.

### Plugin Photon (Multijoueurs)

Pour la partie multijoueur, nous avons utilisé Photon Unity Networking 2. Vous devez aussi l'installer depuis [ici](https://assetstore.unity.com/packages/tools/network/pun-2-free-119922). 

Afin de configurer correctement PUN2, vous devez créer un compte Photon à partir de leur site pour créer une application. Après cela, vous bénéficiez de l'AppID Photon que vous pouvez renseigner après l'installation du plugin PUN2.

#### Remote Procedure Calls

Le jeu utilise des RPC afin de synchroniser les évenements du jeu pour les joueurs. Allez dans le menu Window > Photon Unity Networking > Highlight Server Settings et clquez sur l'asset trouvé.

Dépliez l'option "RPC list" et vérifiez que les méthodes suivantes sont pr&sentes :

- PlayInstrumentSound
- KickPlayer
- SetRoundState
- SetRoundCountdown

Si aucune de celles-ci n'est présente, cliquez sur "Refresh RPCs" ou "Clear RPCs" puis "Refresh RPCS".

#### Pourquoi PUN2?

Ce choix technique a été fait car la stack networking d'Unity, actuellement obsolète, est en train d'être complétement refaite. Afin d'éviter de mauvaises surprises, nous avons choisi une solution bien documentée qui existe depuis plusieurs années.

### Compilation et lancement

Prérequis :

- Oculus Quest en mode développeur
- L'avoir branché à l'ordinateur
- Avoir accepté la signature de l'ordinateur sur le Quest

En allant dans File > Build And Run, vous pouvez compiler et automatiquement lancer le jeu sur le Quest quand il sera prêt.

### Compilation vers APK

Pour lancer la compilation du projet, allez dans File > Build Settings et cliquer sur le bouton "Build". Sauvegardez l'APK à l'emplacement désiré puis prenez votre pause café!

Pour installer l'APK référerez vous au début du readme, dans la section "Installation de l'APK".


