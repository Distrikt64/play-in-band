﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class CustomGrabber : MonoBehaviour
{
    // Grip trigger thresholds for picking up objects, with some hysteresis.
    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    // Current trigger axis value
    private float m_prevFlex;
    private Rigidbody m_grabbedObj = null;
    private Dictionary<Rigidbody, int> m_grabCandidates = new Dictionary<Rigidbody, int>();
    private Rigidbody m_grabCandidate = null;

    private Quaternion m_anchorOffsetRotation;
    private Vector3 m_anchorOffsetPosition;

    // Child/attached Colliders to detect candidate grabbable objects.
    [SerializeField]
    private Collider[] m_grabVolumes = null;

    // Should be OVRInput.Controller.LTouch or OVRInput.Controller.RTouch.
    [SerializeField]
    private OVRInput.Controller m_controller;

    [SerializeField]
    private Transform m_parentTransform;

    void Start()
    {
        if (m_parentTransform == null)
        {
            if (gameObject.transform.parent != null)
            {
                m_parentTransform = gameObject.transform.parent.transform;
            }
            else
            {
                m_parentTransform = new GameObject().transform;
                m_parentTransform.position = Vector3.zero;
                m_parentTransform.rotation = Quaternion.identity;
            }
        }
    }

    void FixedUpdate()
    {
        OnUpdatedAnchors();
    }

    void OnUpdatedAnchors()
    {
        Vector3 handPos = OVRInput.GetLocalControllerPosition(m_controller);
        Quaternion handRot = OVRInput.GetLocalControllerRotation(m_controller);
        Vector3 destPos = m_parentTransform.TransformPoint(handPos);
        Quaternion destRot = m_parentTransform.rotation * handRot;
        GetComponent<Rigidbody>().MovePosition(destPos);
        GetComponent<Rigidbody>().MoveRotation(destRot);

        float prevFlex = m_prevFlex;
        // Update values from inputs
        m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

        CheckForGrabOrRelease(prevFlex);
    }

    /// <summary>
    /// The currently grabbed object.
    /// </summary>
    public Rigidbody grabbedObject
    {
        get { return m_grabbedObj; }
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        // Get the grab trigger
        Rigidbody grabbable = otherCollider.GetComponent<Rigidbody>() ?? otherCollider.GetComponentInParent<Rigidbody>();
        if (otherCollider.GetComponent<Rigidbody>() == null) return;

        m_grabCandidate = grabbable;
    }

    void OnTriggerExit(Collider otherCollider)
    {
        Rigidbody grabbable = otherCollider.GetComponent<Rigidbody>() ?? otherCollider.GetComponentInParent<Rigidbody>();
        if (grabbable == null) return;

        // Remove the grabbable
        m_grabCandidate = null;
    }

    private void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin) && (prevFlex < grabBegin))
        {
            GrabBegin();
        }
        else if ((m_prevFlex <= grabEnd) && (prevFlex > grabEnd))
        {
            GrabEnd();
        }
    }

    private void GrabBegin()
    {
        Rigidbody grabbable = m_grabCandidate;

        if (grabbable.GetComponent<PhotonView>().Owner != PhotonNetwork.LocalPlayer)
        {
            grabbable.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
        }

        if (m_grabbedObj == null)
        {
            m_grabbedObj = m_grabCandidate;

            GetComponent<FixedJoint>().connectedBody = grabbable;
        }
    }

    private void GrabEnd()
    {
        if (m_grabbedObj != null)
        {
            GetComponent<FixedJoint>().connectedBody = null;
            m_grabbedObj = null;
        }
    }
}
