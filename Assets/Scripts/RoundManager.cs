﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoundManager : MonoBehaviourPunCallbacks
{
    private static readonly System.Random randomizer = new System.Random();

    [Tooltip("The text game object to display the countdown")]
    public GameObject countdownDisplay;

    [Tooltip("The text game object to display the players count")]
    public GameObject playersCountDisplay;

    [Tooltip("The public game object with ambient sound")]
    public GameObject ambientPublic;

    [Tooltip("The public game object with applause sound")]
    public GameObject applausePublic;

    [Tooltip("The left curtain game object to open at game start")]
    public GameObject curtainLeft;

    [Tooltip("The right curtain game object to open at game start")]
    public GameObject curtainRight;

    [Tooltip("The minimum room's players count to be ready to play")]
    public int minGamePlayersCount = 2;

    [Tooltip("The duration in seconds before the game starts when ready")]
    public double maxTimeBeforeStart = 60;

    [Tooltip("The duration in seconds before the game starts when ready")]
    public double maxTimeBeforeEnd = 500;

    [Tooltip("The duration in seconds before the game starts when ready")]
    public double maxTimeBeforeKick = 15;

    private enum State { Waiting, Ready, Started, Ending, Ended };
    private State currentState;
    private double currentCountdown;
    private GameObject[] spectators;

    private double remainingTimeBeforeStart;
    private double remainingTimeBeforeEnd;
    private double remainingTimeBeforeKick;

    private bool startedGame;
    private bool endingGame;
    private bool endedGame;

    // Set up countdowns & currentState
    void Start()
    {
        spectators = GameObject.FindGameObjectsWithTag("Spectator");

        remainingTimeBeforeStart = maxTimeBeforeStart;
        remainingTimeBeforeEnd = maxTimeBeforeEnd;
        remainingTimeBeforeKick = maxTimeBeforeKick;

        UpdateRoundPlayersDisplay();

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        photonView.RPC("SetRoundState", RpcTarget.AllBuffered, State.Waiting);

        Debug.Log("[RoundManager] Min players: " + minGamePlayersCount.ToString());
        Debug.Log("[RoundManager] State: " + currentState.ToString());
        Debug.Log("[RoundManager] Current Room Player Count: " + PhotonNetwork.CurrentRoom.PlayerCount.ToString());

        if (PhotonNetwork.CurrentRoom.PlayerCount >= minGamePlayersCount)
        {
            photonView.RPC("SetRoundState", RpcTarget.AllBufferedViaServer, State.Ready);
        }
    }

    // Update remaining time value & display ; sends RPCs to start, end & kick if needed (MasterClient only)
    void Update()
    {
        string countdownTitle = "";

        switch (currentState)
        {
            case State.Ready:
                countdownTitle = "Starting in";
                remainingTimeBeforeStart -= Time.deltaTime;
                remainingTimeBeforeStart = Math.Max(remainingTimeBeforeStart, 0);
                currentCountdown = remainingTimeBeforeStart;
                break;
            case State.Started:
                countdownTitle = "Ending in";
                remainingTimeBeforeEnd -= Time.deltaTime;
                remainingTimeBeforeEnd = Math.Max(remainingTimeBeforeEnd, 0);
                currentCountdown = remainingTimeBeforeEnd;
                break;
            case State.Ending:
                countdownTitle = "Kicked in";
                remainingTimeBeforeKick -= Time.deltaTime;
                remainingTimeBeforeKick = Math.Max(remainingTimeBeforeKick, 0);
                currentCountdown = remainingTimeBeforeKick;
                break;
        }

        UpdateCountdownDisplay(countdownTitle);

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        if (currentState == State.Ready && remainingTimeBeforeStart <= 0.0 && !startedGame)
        {
            Debug.Log("[RoundManager] Start the game");
            startedGame = true;
            PhotonNetwork.CurrentRoom.IsOpen = false;
            photonView.RPC("SetRoundState", RpcTarget.AllViaServer, State.Started);
        }
        else if (currentState == State.Started && remainingTimeBeforeEnd <= 0.0 && !endingGame)
        {
            Debug.Log("[RoundManager] End the game");
            endingGame = true;
            photonView.RPC("SetRoundState", RpcTarget.AllViaServer, State.Ending);
        }
        else if (currentState == State.Ending && remainingTimeBeforeKick <= 0.0 && !endedGame)
        {
            Debug.Log("[RoundManager] Kick the players");
            endedGame = true;
            photonView.RPC("KickPlayer", RpcTarget.All);
        }
    }

    #region PUN Callbacks

    // Update Players Count display & set currentState to Ready via RPC if needed
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        UpdateRoundPlayersDisplay();

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        if (currentState.Equals(State.Waiting) &&
            PhotonNetwork.CurrentRoom.PlayerCount >= minGamePlayersCount)
        {
            photonView.RPC("SetRoundState", RpcTarget.AllBufferedViaServer, State.Ready);
        }
        else
        {
            // New Player can join the room only if current state is Waiting OR Ready
            // Therefore, the only countdown possible here is : remainingTimeBeforeStart
            photonView.RPC("SetRoundState", RpcTarget.AllBufferedViaServer, currentState);
            photonView.RPC("SetTimeToStart", RpcTarget.Others, remainingTimeBeforeStart);
        }
    }

    // Update Players Count display & set currentState to Waiting via RPC if needed
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        UpdateRoundPlayersDisplay();

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        if (currentState == State.Ready &&
            PhotonNetwork.CurrentRoom.PlayerCount < minGamePlayersCount)
        {
            photonView.RPC("SetRoundState", RpcTarget.AllBufferedViaServer, State.Waiting);
        }
    }

    #endregion

    #region RPC Methods

    // Sync countdown for new players
    [PunRPC]
    private void SetTimeToStart(double time)
    {
        Debug.Log("[RoundManager] SetTimeToStart to " + time.ToString());
        remainingTimeBeforeStart = time;
    }

    // Set new State and reset countdowns if needed
    [PunRPC]
    private void SetRoundState(State newState)
    {
        Debug.Log("[RoundManager] SetRoundState to " + newState.ToString());
        State oldState = currentState;
        currentState = newState;

        if (oldState == State.Ready && newState == State.Waiting)
        {
            remainingTimeBeforeStart = maxTimeBeforeStart;
        }

        if (newState == State.Started)
        {
            curtainLeft.GetComponent<Animator>().Play("Opening");
            curtainRight.GetComponent<Animator>().Play("Opening");

            AnimateSpectatorsToApplause();
            applausePublic.GetComponent<AudioSource>().Play();

            Debug.Log("[RoundManager] SetRoundState (Started): Applauding, ready to idle");
            Invoke("AnimateSpectatorsToIdle", applausePublic.GetComponent<AudioSource>().clip.length);

            ambientPublic.GetComponent<AudioSource>().Stop();
        }

        if (newState == State.Ending)
        {
            AnimateSpectatorsToApplause();
            applausePublic.GetComponent<AudioSource>().Play();
            Debug.Log("[RoundManager] SetRoundState (Ending): Applauding, ready to idle");
            Invoke("AnimateSpectatorsToIdle", applausePublic.GetComponent<AudioSource>().clip.length);

            ambientPublic.GetComponent<AudioSource>().Play();
            curtainLeft.GetComponent<Animator>().Play("Closing");
            curtainRight.GetComponent<Animator>().Play("Closing");
        }
    }

    // Kick current player / will be redirected to main menu (see GameManager)
    [PunRPC]
    private void KickPlayer()
    {
        Debug.Log("[RoundManager] KickPlayer");
        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region Animation Methods

    private void AnimateSpectatorsToApplause()
    {
        Debug.Log("[RoundManager] AnimateSpectatorsToApplause (spectators: " + spectators.Length.ToString() + ")");
        foreach (GameObject spectator in spectators)
        {
            spectator.GetComponent<Animator>().CrossFade("armature|sit_applause", 0.25f, -1, 0.0f, GetRandomFloat());
        }
    }

    private void AnimateSpectatorsToIdle()
    {
        Debug.Log("[RoundManager] AnimateSpectatorsToIdle (spectators: " + spectators.Length.ToString() + ")");
        string animationNamePrefix = "armature|sit_idle_";
        string animationName;
        foreach (GameObject spectator in spectators)
        {
            animationName = animationNamePrefix + GetRandomNumber(1, 5);
            Debug.Log("[RoundManager] AnimateSpectatorsToIdle: " + animationName);
            spectator.GetComponent<Animator>().CrossFade(animationName, 0.25f, -1, 0.0f, GetRandomFloat());
        }
    }

    #endregion

    #region Display Methods

    // Update Players Count Display
    private void UpdateRoundPlayersDisplay()
    {
        playersCountDisplay.GetComponent<Text>().text = "Players : " +
            PhotonNetwork.CurrentRoom.PlayerCount.ToString() +
            "/" +
            PhotonNetwork.CurrentRoom.MaxPlayers.ToString();
    }

    // Update Current Countdown Display
    private void UpdateCountdownDisplay(string title)
    {
        if (currentState == State.Waiting)
        {
            countdownDisplay.GetComponent<Text>().text = "Waiting for players...";
        }
        else
        {
            countdownDisplay.GetComponent<Text>().text = title + " " + String.Format("{0:00}", currentCountdown);
        }
    }

    #endregion

    #region Static Methods

    public static float GetRandomFloat()
    {
        lock (randomizer) // synchronize
        {
            return (float) randomizer.NextDouble();
        }
    }

    public static int GetRandomNumber(int min, int max)
    {
        lock (randomizer) // synchronize
        {
            return randomizer.Next(min, max);
        }
    }

    #endregion
}
