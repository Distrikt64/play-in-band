﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CurtainController : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        // Open curtain on GameScene
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            anim.Play("Opening");
        }
    }
}
