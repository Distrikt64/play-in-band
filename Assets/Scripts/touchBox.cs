﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class touchBox : MonoBehaviour
{
    public AudioClip sound;
    public GameObject star_animation;

    private AudioSource source;
    private PhotonView photonView;

    public float velToVol = 1f;

    private float volLowRange = .5f;
    private float volHighRange = 1f;

    public float lowPitchRange = .975f;
    public float highPitchRange = 1.025f;

    public bool checkAngle = true;

    private Vector3 validDirection = Vector3.down;
    private float contactThreshold = 30;          // Acceptable collision angle difference in degrees

    private Collision coll;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        photonView = GetComponent<PhotonView>();
    }
   
    void OnCollisionEnter (Collision coll)
    {   
        for (int k = 0; k < coll.contacts.Length; k++)
        {
            if (!checkAngle || Vector3.Angle(coll.contacts[k].normal, validDirection) <= contactThreshold)
            {
                // Collided with a surface facing mostly upwards
                this.coll = coll;
                photonView.RPC("PlayInstrumentSound", RpcTarget.All);
                animateBox(coll.GetContact(0).point);
                break;
            }
        } 
    }

    [PunRPC]
    public void PlayInstrumentSound()
    {
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        source.PlayOneShot(sound, this.coll.relativeVelocity.magnitude * velToVol);
    }

     void animateBox(Vector3 point)
    {
        if (star_animation == null)
        {
            Debug.Log("[touchBox] No animation");
            return;
        }

        GameObject tmp = Instantiate(star_animation, point, Quaternion.identity);
        Destroy(tmp, 5); // time in seconds
    }
}