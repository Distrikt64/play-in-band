﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

namespace Com.PlayInBandStudios.PlayInBand
{

    public class GameManager : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        [Tooltip("The prefab to use for representing the local player")]
        public GameObject localPlayerPrefab;


        [Tooltip("The prefab to use for representing the remote player")]
        public GameObject remotePlayerPrefab;

        private readonly byte InstantiateVrAvatarEventCode = 123;

        #region Photon Callbacks
        public void Start()
        {
            if (localPlayerPrefab == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
            } else
            {
                GameObject localPlayer = Instantiate(this.localPlayerPrefab, new Vector3(0f, 5f, 0f), Quaternion.identity) as GameObject;
                PhotonView photonView = localPlayer.GetComponentInChildren<PhotonView>();

                if (PhotonNetwork.AllocateViewID(photonView))
                {
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                    {
                        CachingOption = EventCaching.AddToRoomCache,
                        Receivers = ReceiverGroup.Others
                    };

                    SendOptions sendOptions = new SendOptions
                    {
                        Reliability = true
                    };

                    PhotonNetwork.RaiseEvent(InstantiateVrAvatarEventCode, photonView.ViewID, raiseEventOptions, sendOptions);
                }
                else
                {
                    Debug.LogError("Failed to allocate a ViewId.");

                    Destroy(localPlayer);
                }
            }
        }

        public void OnEvent(EventData photonEvent)
        {
            if (photonEvent.Code == InstantiateVrAvatarEventCode)
            {
                GameObject remotePlayer = Instantiate(this.remotePlayerPrefab, new Vector3(0f, 5f, 0f), Quaternion.identity) as GameObject;
                PhotonView photonView = remotePlayer.GetComponent<PhotonView>();
                photonView.ViewID = (int)photonEvent.CustomData;
            }
        }

        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.LogFormat("OnPlayerEnteredRoom(): {0}", newPlayer.NickName);
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.LogFormat("OnPlayerLeftRoom(): {0}", otherPlayer.NickName);
        }

        public override void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        public override void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        #endregion
    }

}