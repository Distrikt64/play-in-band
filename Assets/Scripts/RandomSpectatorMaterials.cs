﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpectatorMaterials : MonoBehaviour
{
    void Start()
    {
        GetComponent<SkinnedMeshRenderer>().materials[1].color = Random.ColorHSV(0f, 1f, 1f, 1f, .1f, .6f);
        GetComponent<SkinnedMeshRenderer>().materials[2].color = Random.ColorHSV(0f, 1f, 1f, 1f, .1f, .6f);
        GetComponent<SkinnedMeshRenderer>().materials[3].color = Random.ColorHSV(0f, 1f, 1f, 1f, .1f, .6f);
    }
}
